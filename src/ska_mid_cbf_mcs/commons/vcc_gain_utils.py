# -*- coding: utf-8 -*-
#
# This file is part of the SKA Mid.CBF TDC MCS project
#
# Distributed under the terms of the GPL license.
# See LICENSE for more info.

"""This module implements utilities for gain correction of VCC frequency response"""

from __future__ import annotations  # allow forward references in type hints

import numpy as np
import scipy
import yaml

from ska_mid_cbf_mcs.commons.global_enum import (
    calculate_dish_sample_rate,
    const,
    freq_band_dict,
)

# YAML file containing the finite impulse response (FIR) data for a VCC
VCC_FIR_PATH = "mnt/vcc_param/VCC_FIR.yml"

DEFAULT_GAIN = 1.0
MIN_GAIN = 0.0
MAX_GAIN = 4.005


def get_vcc_ripple_correction(
    freq_band: str,
    scfo_fsft: int,
    freq_offset_k: int,
) -> list[float]:
    """
    Calculates fine channel gains to correct for VCC pass-band ripple.
    Based on https://gitlab.com/ska-telescope/ska-mid-cbf-signal-verification/-/blob/main/images/ska-mid-cbf-signal-verification/hardware_testing_notebooks/talon_pyro/talon_FSP.py

    From Will Kamp:
    The Very Coarse Channeliser (VCC) introduces a variation in gain across its pass band for each channel,
    also known as pass-band ripple. This is introduced by its finite impulse response (FIR) filter.
    Knowing the FIR filter's impulse response (filter taps) we can work out the variation in gain for
    each of the subsequent fine channels generated by the imaging (16k fine) channeliser.
    We seek to  correct the magnitude of each fine channel, so that if a spectrally flat signal is applied,
    then the average magnitude of the fine channels is the same.

    Algorithm:
    - Calculate the center frequency of the frequency slice (coarse channel) being corrected.
      This depends on the coarse channel, the input sample rate to the VCC before resampling,
      the resampled common sample rate into the fine channeliser, and any wideband or
      resampler frequency shifting applied.
    - Use the center frequency to locate the fine-channel frequencies at the VCC input sample rate
      (prior to resampling).
    - Use the freqz function (from the scipy.signal, or matlab.signal modules) to convert
      the FIR filter impulse response to magnitude responses at the fine channel frequencies.
    - Invert the magnitude responses to create corrective gains.
    - Apply the gains in the imaging channeliser.

    :param freq_band: the frequency band of the VCC
    :param scfo_fsft: the frequency shift of the RDT required due to Sample Clock
        Frequency Offset (SCFO) sampling
    :param freq_offset_k: the frequency offset k value of the DISH/VCC
    :return: list of corrective fine channel gain values
    """

    # Load VCC band info to calculate FS sample rate
    freq_band_info = freq_band_dict()[freq_band]
    input_sample_rate = calculate_dish_sample_rate(
        freq_band_info=freq_band_info, freq_offset_k=freq_offset_k
    )
    input_frame_size = freq_band_info["num_samples_per_frame"]
    frequency_slice_sample_rate = input_sample_rate // input_frame_size

    # Calculate normalized actual center frequency of secondary channelizer
    fc0 = np.linspace(
        start=-1,
        stop=1 - 2 / const.TOTAL_FINE_CHANNELS,
        num=const.TOTAL_FINE_CHANNELS,
    )
    actual_center_frequency = fc0 * const.COMMON_SAMPLE_RATE / 2 - scfo_fsft
    normalized_center_frequency = (
        actual_center_frequency
        / frequency_slice_sample_rate
        / input_frame_size
    )

    # Evaluate VCC frequency response data
    with open(f"{VCC_FIR_PATH}", "r") as file:
        vcc_fir = yaml.safe_load(file)
    vcc_fir_coeff = vcc_fir["h"]

    # Convert impulse response data to magnitude responses
    _, fr_values = scipy.signal.freqz(
        vcc_fir_coeff, a=1, worN=2 * np.pi * normalized_center_frequency
    )

    # Invert magnitude responses to create corrective gains
    vcc_gain_corrections = np.clip(
        DEFAULT_GAIN / abs(fr_values), a_min=MIN_GAIN, a_max=MAX_GAIN
    ).tolist()

    # FFT-shift to match ordering of fine channelizer registers
    vcc_gains_copy = list(vcc_gain_corrections)
    center_channel = const.TOTAL_FINE_CHANNELS // 2
    vcc_gain_corrections = (
        vcc_gains_copy[center_channel:] + vcc_gains_copy[:center_channel]
    )

    return vcc_gain_corrections
