# -*- coding: utf-8 -*-
#
# This file is part of the SKA Mid.CBF MCS project
#
# Ported from the SKA Low MCCS project:
# https://gitlab.com/ska-telescope/ska-low-mccs/-/blob/main/testing/src/tests/unit/conftest.py
#
# Distributed under the terms of the GPL license.
# See LICENSE for more info.

"""This module contains pytest-specific test harness for MCS unit tests."""
