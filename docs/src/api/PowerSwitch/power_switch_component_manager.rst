PowerSwitchComponentManager Class
---------------------------------

.. autoclass:: ska_mid_cbf_mcs.power_switch.power_switch_component_manager.PowerSwitchComponentManager
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: