PowerSwitch Device
------------------

.. autoclass:: ska_mid_cbf_mcs.power_switch.power_switch_device.PowerSwitch
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: