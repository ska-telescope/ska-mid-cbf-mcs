PowerSwitchSimulator
--------------------

.. automodule:: ska_mid_cbf_mcs.power_switch.power_switch_simulator
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: