TalonLRU Device
---------------

.. autoclass:: ska_mid_cbf_mcs.talon_lru.talon_lru_device.TalonLRU
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: