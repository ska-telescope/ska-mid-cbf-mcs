VCC Class
---------------------------

.. autoclass:: ska_mid_cbf_mcs.vcc.vcc_device.Vcc
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: