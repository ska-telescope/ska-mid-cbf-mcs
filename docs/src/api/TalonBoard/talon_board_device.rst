TalonBoard Device
---------------

.. autoclass:: ska_mid_cbf_mcs.talon_board.talon_board_device.TalonBoard
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: