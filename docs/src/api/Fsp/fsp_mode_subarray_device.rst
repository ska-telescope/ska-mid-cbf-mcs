FspModeSubarray Class
---------------------------

.. autoclass:: ska_mid_cbf_mcs.fsp.fsp_mode_subarray_device.FspModeSubarray
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order:
