CbfController Class
---------------------------------
.. autoclass:: ska_mid_cbf_mcs.controller.controller_device.CbfController
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: