==================
CbfSubarray
==================

.. automodule:: ska_mid_cbf_mcs.subarray


.. toctree::

  CbfSubarray Device<cbfsubarray_device>
  CbfSubarrayComponentManager<cbfsubarray_component_manager>