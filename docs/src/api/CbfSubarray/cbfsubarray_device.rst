CbfSubarray Class
-----------------

.. autoclass:: ska_mid_cbf_mcs.subarray.subarray_device.CbfSubarray
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: