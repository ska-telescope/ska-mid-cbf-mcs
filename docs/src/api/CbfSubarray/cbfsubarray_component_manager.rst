CbfSubarray Component Class
-------------------------------

.. autoclass:: ska_mid_cbf_mcs.subarray.subarray_component_manager.CbfSubarrayComponentManager
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: