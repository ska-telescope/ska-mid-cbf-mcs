Slim Class
----------

.. autoclass:: ska_mid_cbf_mcs.slim.slim_device.Slim
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: