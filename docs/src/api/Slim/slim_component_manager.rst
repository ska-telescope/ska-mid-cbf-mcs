SlimComponentManager Class
--------------------------

.. autoclass:: ska_mid_cbf_mcs.slim.slim_component_manager.SlimComponentManager
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order: