==================
Slim 
==================

.. automodule:: ska_mid_cbf_mcs.slim


.. toctree::

  Slim<slim_device>
  SlimComponentManager<slim_component_manager>
  SlimLink<slim_link_device>
  SlimLinkComponentManager<slim_link_component_manager>

