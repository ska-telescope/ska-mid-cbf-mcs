MID CBF MCS Documentation
========================================

.. BACKGROUND =============================================================
.. toctree::
   :maxdepth: 2
   :caption: Background

   guide/system_context

.. INTERFACES =============================================================
.. toctree::
   :maxdepth: 2
   :caption: Interfaces

   guide/interfaces/lmc_mcs_interface
   guide/interfaces/mcs_hps_interface
   guide/interfaces/subscription_points

.. DEVELOPER =============================================================
.. toctree::
   :maxdepth: 2
   :caption: Developer Guide

   Getting Started<guide/developer/getting_started/index>
   Design Notes<guide/developer/design_notes>

.. API =============================================================
.. toctree::
   :maxdepth: 2
   :caption: ska-mid-cbf-mcs API

   api/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
