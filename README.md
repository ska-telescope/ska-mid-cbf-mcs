# Mid.CBF MCS

Code repository: [ska-mid-cbf-mcs](https://gitlab.com/ska-telescope/ska-mid-cbf-mcs)

# Table Of Contents
* [Cloning the Repository](#cloning-the-repository)
* [Running the Mid CBF MCS](#running-the-mid-cbf-mcs)

# Cloning the Repository

Clone the repository with the following command in the directory of your choice:
```
git clone https://gitlab.com/ska-telescope/ska-mid-cbf-mcs.git
```
Navigate to the newly cloned directory and initialize and update the git submodules:
```
git submodule init
git submodule update
```

# Running the Mid CBF MCS

Follow the MCS ReadTheDocs guide to deploy the MCS:
[ReadTheDocs](https://developer.skao.int/projects/ska-mid-cbf-mcs/en/latest/guide/developer/getting_started/index.html)

