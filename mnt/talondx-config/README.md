# Internal HPS Master Configuration File

This directory contains the talondx-config.json configuration file used by the CbfController
and HPSMaster devices, which provides information on the current Talon-DX software/firmware
configuration, including;
* The target Talon ID(s), IP address(es) and timeout length when establishing connection
* The HPS Master, Talon LRU and RDMA Rx device FQDNs
* Path and names of FPGA and HPS device server files to use

