# Serial Lightweight Interconnect Mesh (SLIM) configuration File

This directory contains the fs_slim_config.yaml configuration file used by the
MCS to determine the connections in the frequency slice mesh. Each 
connection is defined by the Tango device FQDN of the Tx and Rx of the link. 
If the link is not active, it is marked with [x].
