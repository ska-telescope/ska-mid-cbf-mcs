# Hardware Definition File

This directory contains the hw_config.yaml configuration file used by the CbfController
to dictate what physical hardware targets/addresses correspond to which software
devices deployed in MCS; in particular, it will allow mapping of physical
Talon hardware to Talon board and LRU Tango devices, physical power switches to 
the power switch devices.

The hw_config.yaml checked in here (ska-mid-cbf-mcs) is only an example of the format,
and is not maintained to reflect the latest hardware configuration for any environment.
